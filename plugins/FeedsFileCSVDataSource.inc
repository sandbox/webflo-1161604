<?php

/**
 * Parses a given file as a CSV file.
 */
class FeedsFileCSVDataSource extends FeedsCSVParser {

  /**
   * Implementation of FeedsParser::parse().
   */
  public function parse(FeedsImportBatch $batch, FeedsSource $source) {

    // Load and configure parser.
    module_load_include('php', 'feeds_csv', 'libraries/File_CSV_DataSource/DataSource');
    $source_config = $source->getConfigFor($this);
    $delimiter = $source_config['delimiter'] == 'TAB' ? "\t" : $source_config['delimiter'];
    
    $parser = new File_CSV_DataSource;
    $parser->settings(array('delimiter' => $delimiter));
    $parser->load(realpath($batch->getFilePath()));
    
    $header = array();
    if (empty($source_config['no_headers'])) {
      // Get first line and use it for column names
      $header = $parser->getHeaders();
      
      if (!$header) {
        return;
      }
      
      // convert them to lower case.
      foreach($header as &$col) {
        $col = drupal_strtolower($col);
      }
    }
    
    $results = array();
    
    // create lower array keys for mapping.
    foreach($parser->connect() as $item) {
      $results[] = array_change_key_case($item, CASE_LOWER);
    }
    
    // Populate batch.
    $batch->items = $results;
  }
}